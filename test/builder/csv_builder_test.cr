require "minitest/autorun"

require "/../src/builder/csv_builder"

class CSVBuilderTest < Minitest::Test
  def test_builds_an_empty_csv
    dataframe = Dataframe.new
    csv = dataframe.to_csv

    assert_equal "", csv
  end

  def test_builds_csv
    csv = File.read("./test/files/adults.csv")
    dataframe = Dataframe.from_csv(csv)

    assert_equal csv, dataframe.to_csv
  end
end
