require "csv"

require "../common"

class Dataframe
  class CSVBuilder < CSV::Builder
    @quoting = Quoting::RFC

    # Creates a builder that will write to the given `IO`.
    def initialize(@io : IO, @separator : Char = DEFAULT_SEPARATOR, @quote_char : Char = DEFAULT_QUOTE_CHAR)
      @first_cell_in_row = true
    end

    def build(headers : Array(String), rows : Array(Array(Type)))
      add_row headers
      rows.each do |row|
        add_row row
      end
    end

    # Appends the given values as a single row, and then a newline.
    def add_row(values : Array(Type)) : Nil
      values.each do |value|
        if value.is_a?(String)
          quote_cell value
        else
          cell { |io| io << value }
        end
      end
      @io << '\n'
      @first_cell_in_row = true
    end
  end

  # :nodoc:
  def quote_cell(value : String)
    append_cell do
      @io << @quote_char
      @io << value
      @io << @quote_char
    end
  end
end
